﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using VOD.Common.DTOModel;
using VOD.Common.Entities;
using VOD.Database.Service;

namespace VOD.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AuthorController : Controller
    {
        private readonly string _userId;
        private readonly IUIReadService _dbRead;
        private readonly IUIWriteService _dbWrite;

        public AuthorController(IHttpContextAccessor httpContextAccessor,
            UserManager<VODUser> userManager, IUIReadService dbRead, IUIWriteService dbWrite)
        {
            var user = httpContextAccessor.HttpContext.User;
            _userId = userManager.GetUserId(user);
            _dbRead = dbRead;
            _dbWrite = dbWrite;
        }


        // GET: api/<controller>
        [HttpGet]
        public async Task<ActionResult<List<AuthorDTO>>> Get(bool include = false)
        {
            try
            {
                return await _dbRead.GetAsync<Author, AuthorDTO>(include);
            }
            catch (Exception)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, "Database Failure");
            }
        }
    }
}