﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Routing;
using VOD.Common.DTOModel;
using VOD.Common.Entities;
using VOD.Database.Service;

namespace VOD.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class BookController : Controller
    {
        private readonly string _userId;
        private readonly IUIReadService _dbRead;
        private readonly IUIWriteService _dbWrite;

        private readonly LinkGenerator _linkGenerator;

        public BookController(IHttpContextAccessor httpContextAccessor,
            UserManager<VODUser> userManager,IUIReadService dbRead, IUIWriteService dbWrite, LinkGenerator linkGenerator)
        {
            var user = httpContextAccessor.HttpContext.User;
            _userId = userManager.GetUserId(user);
            _dbRead = dbRead;
            _dbWrite = dbWrite;
            _linkGenerator = linkGenerator;
        }

        // GET: api/<controller>
        [HttpGet]
        public async Task<ActionResult<List<BookDTO>>> Get(bool include = false)
        {
            try
            {
                return await _dbRead.GetAsync<Book, BookDTO>(include);
            }
            catch (Exception)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, "Database Failure");
            }
        }
        [HttpGet("{id:int}")]
        public async Task<ActionResult<BookDTO>> Get(int ID, bool include = false)
        {
            try
            {
                return await _dbRead.GetSingleAsync<Book, BookDTO>(x => x.ID.Equals(ID), include);
            }
            catch (Exception)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, "Database Failure");
            }
        }
        public async Task<ActionResult<BookDTO>> Post(BookDTO model)
        {
            try
            {
                if (model == null)
                {
                    return BadRequest("No entity provided");
                }
                var id = await _dbWrite.CreateAsync<BookDTO, Book>(model);

                var dto = await _dbRead.GetSingleAsync<Book, BookDTO>(s => s.ID.Equals(id));

                if (dto == null) { return BadRequest("Unable to add the entity"); }

                var uri = _linkGenerator.GetPathByAction("Get", "Book", new { id });

                return Created(uri, dto);

            }
            catch (Exception)
            {

                return StatusCode(StatusCodes.Status500InternalServerError, "Database Failure");
            }
        }
        // PUT api/<controller>/5
        [HttpPut("{id}")]
        public async Task<IActionResult> Put(int id, BookDTO model)
        {
            try
            {
                if (model == null) return BadRequest("Missing entity");
                if (!id.Equals(model.ID)) return BadRequest("Differing ids");

                //var exists = await _db.AnyAsync<Instructor>(a => a.Id.Equals(courseDTO.InstructorId));

                //if (!exists)
                //{
                //    return BadRequest("Could not find related entity");
                //}

                //exists = await _db.AnyAsync<Course>(a => a.Id.Equals(id));

                //if (!exists)
                //{
                //    return BadRequest("Could not find related entity");
                //}
                if (await _dbWrite.UpdateAsync<BookDTO, Book>(model))
                {
                    return NoContent();
                }

            }
            catch (Exception)
            {

                return StatusCode(StatusCodes.Status500InternalServerError, "Database Failure");
            }
            return BadRequest("Could not find related entity");
        }
        // DELETE api/<controller>/5
        [HttpDelete("{id:int}")]
        public async Task<IActionResult> Delete(int id)
        {
            try
            {
                //var exists = await _dbRead.AnyAsync<Book>(a =>
                //    a.Id.Equals(id));

                //if (!exists) return BadRequest("Could not find entity");

                if (await _dbWrite.DeleteAsync<Book>(d => d.ID.Equals(id)))
                {
                    return NoContent();
                }
            }
            catch (Exception)
            {

                return StatusCode(StatusCodes.Status500InternalServerError, "Database Failure");
            }
            return BadRequest("Failed to delete the entity");
        }
    }
}