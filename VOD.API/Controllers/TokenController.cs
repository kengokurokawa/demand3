﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using VOD.API.Service;
using VOD.Common.DTOModel;

namespace VOD.API.Controllers
{
    [Route("api/token")]
    [ApiController]
    public class TokenController : ControllerBase
    {
        private readonly ITokenService _tokenService;

        public TokenController(ITokenService tokenService)
        {
            _tokenService = tokenService;
        }
        [HttpPost]
        public async Task<ActionResult<TokenDTO>> GenerateTokenAsync(LoginUserDTO loginUserDTO)
        {
            try
            {
                var jwt = await _tokenService.GenerateTokenAsync(loginUserDTO);

                if (jwt.Token == null) return Unauthorized();

                return jwt;
            }
            catch (Exception)
            {

                return Unauthorized();
            }
        }
        [HttpGet("{userId}")]
        public async Task<ActionResult<TokenDTO>> GetTokenAsync(string userId, LoginUserDTO loginUserDTO)
        {
            try
            {
                var jwt = await _tokenService.GetTokenAsync(loginUserDTO, userId);

                if (jwt.Token == null) return Unauthorized();

                return jwt;
            }
            catch (Exception)
            {

                return Unauthorized();
            }
        }

    }
}