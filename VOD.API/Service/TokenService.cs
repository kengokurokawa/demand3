﻿using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using VOD.Common.DTOModel;
using VOD.Common.Entities;
using VOD.Database.Service;

namespace VOD.API.Service
{
    public class TokenService : ITokenService
    {
        private readonly IConfiguration _configuration;
        private readonly IUserService _users;

        public TokenService(IConfiguration configuration,
            IUserService userService)
        {
            _configuration = configuration;
            _users = userService;
        }

        public async Task<TokenDTO> GenerateTokenAsync(LoginUserDTO loginUserDTO)
        {
            try
            {
                var user = await _users.GetUserAsync(loginUserDTO, true);

                if(user == null) { throw new UnauthorizedAccessException(); }

                var claims = GetClaims(user, true);

                var token = CreateToken(claims);

                var succeeded = await AddTokenToUserAsync(user.Id, token);

                if(!succeeded)
                {
                    throw new SecurityTokenException("Could not add a token to the user");
                }

                return token;
            }
            catch (Exception ex)
            {

                throw;
            }
        }

        public async Task<TokenDTO> GetTokenAsync(LoginUserDTO loginUserDTO, string userId)
        {
            try
            {
                var user = await _users.GetUserAsync(loginUserDTO, true);

                if (user == null)
                {
                    throw new UnauthorizedAccessException();
                }

                if (!userId.Equals(user.Id))
                {
                    throw new UnauthorizedAccessException();
                }

                return new TokenDTO(user.Token, user.TokenExpires);
            }
            catch (Exception)
            {

                throw;
            }
        }

        private List<Claim> GetClaims(VODUser user, bool includeUserClaims)
        {
            var claims = new List<Claim>
            {
                new Claim(JwtRegisteredClaimNames.Sub, user.UserName),
                new Claim(JwtRegisteredClaimNames.Email, user.Email),
                new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString()),
            };

            if(includeUserClaims)
            {
                foreach(var claim in user.Claims)
                {
                    if(!claim.Type.Equals("Token") &&
                       !claim.Type.Equals("TokenExpires"))
                    {
                        claims.Add(claim);
                    }
                }
            }
            return claims;
        }
        private TokenDTO CreateToken(IList<Claim> claims)
        {
            try
            {
                var signinKey = Convert.FromBase64String(_configuration["Jwt:SigninSecret"]);

                var credentials = new SigningCredentials(new SymmetricSecurityKey(signinKey),
                    SecurityAlgorithms.HmacSha256Signature);

                var duration = int.Parse(_configuration["Jwt:Duration"]);

                var now = DateTime.UtcNow;

                var jwtToken = new JwtSecurityToken
                (
                    issuer:"http://csharpschool.com",
                    audience: "http://csharpschool.com",
                    notBefore:now,
                    claims:claims,
                    signingCredentials: credentials
                 );

                var jwtTokenHandler = new JwtSecurityTokenHandler();
                var token = jwtTokenHandler.WriteToken(jwtToken);
                return new TokenDTO(token, jwtToken.ValidTo);
            }
            catch (Exception　ex)
            {

                throw;
            }
        }
        private async Task<bool> AddTokenToUserAsync(string userId, TokenDTO token)
        {
            var userDTO = await _users.GetUserAsync(userId);
            userDTO.Token.Token = token.Token;
            userDTO.Token.TokenExpires = token.TokenExpires;

            return await _users.UpdateUserAsync(userDTO);
        }
    }
}
