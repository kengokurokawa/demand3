﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using VOD.Common.DTOModel;

namespace VOD.API.Service
{
    public interface ITokenService
    {
        Task<TokenDTO> GenerateTokenAsync(LoginUserDTO loginUserDTO);
        Task<TokenDTO> GetTokenAsync(LoginUserDTO loginUserDTO, string userId);
    }
}
