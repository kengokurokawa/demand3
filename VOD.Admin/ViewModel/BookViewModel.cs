﻿using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using VOD.Common.DTOModel;

namespace VOD.Admin.ViewModel
{
    public class BookViewModel
    {
        public IEnumerable<BookDTO> Books { get; set; }
        public BookDTO Book { get; set; }

        public int Author_ID { get; set; }


        public SelectList AuthorsList { get; set; }
    }
}
