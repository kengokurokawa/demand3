﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.UI;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using VOD.Admin.Data;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using VOD.Database.Context;
using VOD.Common.Entities;
using VOD.Common.DTOModel;
using VOD.Database.Service;
using VOD.Common.Service;
using System.Net.Http;

namespace VOD.Admin
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.Configure<CookiePolicyOptions>(options =>
            {
                // This lambda determines whether user consent for non-essential cookies is needed for a given request.
                options.CheckConsentNeeded = context => true;
                options.MinimumSameSitePolicy = SameSiteMode.None;
            });

            services.AddDbContext<VODContext>(options =>
            options.UseSqlServer(
            Configuration.GetConnectionString("Default")));

            services.AddDefaultIdentity<VODUser>()
                    .AddRoles<IdentityRole>()
                    .AddEntityFrameworkStores<VODContext>();

            services.AddScoped<IDbReadService, DbReadService>();
            services.AddScoped<Database.Service.IUIReadService, UIReadService>();
            services.AddScoped<IDbWriteService, DbWriteService>();
            services.AddScoped<Database.Service.IUIWriteService, UIWriteService>();
            services.AddScoped<Common.Service.IUIWriteService, ApiWriteService>();
            services.AddScoped<Common.Service.IUIReadService, ApiReadService>();
            services.AddScoped<IHttpClientFactoryService, HttpClientFactoryService>();
            services.AddScoped<IUserService, UserService>();

            var config = new AutoMapper.MapperConfiguration(cfg =>
            {
                cfg.CreateMap<Book, BookDTO>();
                   //.ReverseMap()
                   //.ForMember(d => d.RowVersion, a => a.Ignore());
                cfg.CreateMap<Author, AuthorDTO>()
                .ReverseMap()
                .ForMember(d => d.Books, a => a.Ignore());
            });
            var mapper = config.CreateMapper();
            services.AddSingleton(mapper);

            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_2);

            services.AddHttpClient("AdminClient", client =>
            {
                client.BaseAddress = new Uri("http://localhost:6600");
                client.Timeout = new TimeSpan(0, 0, 30);
                client.DefaultRequestHeaders.Clear();

            }).ConfigureHttpMessageHandlerBuilder(h =>
                new HttpClientHandler()
                {
                    AutomaticDecompression = System.Net.DecompressionMethods.GZip
                });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, IServiceProvider service)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseDatabaseErrorPage();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }

            app.UseHttpsRedirection();
            app.UseStaticFiles();
            app.UseCookiePolicy();

            app.UseAuthentication();

            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "default",
                    template: "{controller=Home}/{action=Index}/{id?}");
            });
            //CreateUserRoles(service).Wait();
        }
        private async Task CreateUserRoles(IServiceProvider serviceProvider)
        {
            var RoleManager = serviceProvider.GetRequiredService<RoleManager<IdentityRole>>();
            var UserManager = serviceProvider.GetRequiredService<UserManager<VODUser>>();

            IdentityResult roleResult;
            //Adding Admin Role
            var roleCheck = await RoleManager.RoleExistsAsync("Admin");
            if (!roleCheck)
            {
                //create the roles and seed them to the database
                roleResult = await RoleManager.CreateAsync(new IdentityRole("Admin"));
            }
            //Assign Admin role to the main User here we have given our newly registered 
            //login id for Admin management
            VODUser user = await UserManager.FindByEmailAsync("admin03@admin");
            var User = new VODUser();
            await UserManager.AddToRoleAsync(user, "Admin");
        }
    }
}
