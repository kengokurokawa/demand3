﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using VOD.Admin.ViewModel;
using VOD.Common.DTOModel;
using VOD.Common.Entities;
using VOD.Database.Service;
using X.PagedList;
using VOD.Common.Extensions;

namespace VOD.Admin.Controllers
{
    public class BookController : Controller
    {

        private readonly string _userId;

        private readonly IMapper _mapper;

        private readonly VOD.Common.Service.IUIReadService _dbRead;
        private readonly VOD.Common.Service.IUIWriteService _dbWrite;


        public BookController(IHttpContextAccessor httpContextAccessor,
            UserManager<VODUser> userManager, IMapper mapper, VOD.Common.Service.IUIReadService dbRead, VOD.Common.Service.IUIWriteService dbWrite)
        {
            var user = httpContextAccessor.HttpContext.User;
            _userId = userManager.GetUserId(user);
            _mapper = mapper;
            _dbRead = dbRead;
            _dbWrite = dbWrite;
        }
        [HttpGet]
        public async Task<IActionResult> BookList(int? id, int? authId)
        {

            Dictionary<string, object> _properties = new Dictionary<string, object>();
            _properties.Add("ID", 2);


            var booksDto = await _dbRead.GetAsync<Book, BookDTO>(true);
            var authorDto = await _dbRead.GetAsync<Author, AuthorDTO>(true);
            SelectList selectList = new SelectList(authorDto, "ID", "Name");
            //var AuthorDto = await _dbRead.GetAsync<Author, AuthorDTO>(true);
            var pageSize = 20;
            var pageNum = (id ?? 1);

            BookViewModel vm = new BookViewModel();
            vm.Books = await booksDto.ToPagedListAsync(pageNum, pageSize);
            vm.AuthorsList = selectList;
            if (authId.HasValue && authId.Value > 0)
            {
                string[] dataSource =
                    { "1", "orange", "peach", "melon", "grape" };
                var result = booksDto.AsQueryable().Where(LinqExpressionExtension.GetExpressionTreeWhere<BookDTO>(15));

                if(result.Any())
                {
                    string s = "";
                }

                vm.Author_ID = authId.Value;
                var ret = booksDto.Where(item => item.ID.Equals(15));

                vm.Books = await booksDto.Where(x => x.Author_ID.Equals(authId)).ToPagedListAsync(pageNum, pageSize);
                vm.AuthorsList = new SelectList(authorDto, "ID", "Name", authId);
            }
            return View(vm);

        }
        [HttpPost]
        public async Task<IActionResult> BookList([Bind("Author_ID")]BookDTO book, int? id)
        {

            var booksDto = await _dbRead.GetAsync<Book, BookDTO>(true);
            var authorDto = await _dbRead.GetAsync<Author, AuthorDTO>(true);
            SelectList selectList = new SelectList(authorDto, "ID", "Name");
            //var AuthorDto = await _dbRead.GetAsync<Author, AuthorDTO>(true);
            var pageSize = 20;
            var pageNum = (id ?? 1);

            BookViewModel vm = new BookViewModel();
            vm.Author_ID = book.Author_ID;
            vm.Books = await booksDto.Where(x => x.Author_ID.Equals(book.Author_ID)).ToPagedListAsync(pageNum, pageSize);
            vm.AuthorsList = selectList;
            return View(vm);

        }
        [HttpGet]
        public async Task<IActionResult> Create()
        {
            var authorDto = await _dbRead.GetAsync<Author, AuthorDTO>(true);
            SelectList selectList = new SelectList(authorDto, "ID", "Name");

            var bookVieModel = new BookViewModel()
            {
                AuthorsList = selectList
            };

            return View(bookVieModel);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("ID, Title, Price, Publisher, Sample, ReleaseDate, Author_ID")]BookDTO book)
        {
            if (ModelState.IsValid)
            {
                book.CreateDate = DateTime.Now;
                book.UpdateDate = DateTime.Now;
                await _dbWrite.CreateAsync<BookDTO, Book>(book);
                return RedirectToAction(nameof(BookList));
            }
            var authorDto = await _dbRead.GetAsync<Author, AuthorDTO>(true);
            SelectList selectList = new SelectList(authorDto, "ID", "Name");
            var bookVieModel = new BookViewModel()
            {
                Book = book,
                AuthorsList = selectList
            };
            return View(bookVieModel);
        }

        [HttpGet]
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }
            //var book = await _dbRead.GetBook(id);
            //if (book == null)
            //{
            //    return NotFound();
            //}
            int ID = id.Value;
            var bookDto = await _dbRead.GetSingleAsync<Book, BookDTO>(x => x.ID.Equals(ID), true);
            if (bookDto == null)
            {
                return NotFound();
            }

            var authorDto = await _dbRead.GetAsync<Author, AuthorDTO>(true);
            SelectList selectList = new SelectList(authorDto, "ID", "Name", authorDto.Single(x => x.ID.Equals(bookDto.Author_ID)));
            var bookVieModel = new BookViewModel()
            {
                Book = bookDto,
                AuthorsList = selectList
            };
            return View(bookVieModel);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("ID, Title, Price, Publisher, Sample, ReleaseDate, Author_ID,RowVersion")]BookDTO book)
        {
            if (book.ID != id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                if (book.CreateDate == null) { book.CreateDate = DateTime.Now; }
                book.UpdateDate = DateTime.Now;

                await _dbWrite.UpdateAsync<BookDTO, Book>(book);
                return RedirectToAction(nameof(BookList));
            }
            var authorDto = await _dbRead.GetAsync<Author, AuthorDTO>(true);

            SelectList selectList = new SelectList(authorDto, "ID", "Name", authorDto.Single(x => x.ID.Equals(book.Author_ID)));
            var bookVieModel = new BookViewModel()
            {
                Book = book,
                AuthorsList = selectList
            };
            return View(bookVieModel);

        }
        [HttpGet]
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }
            else
            {
                int ID = id.Value;
                var bookDto = await _dbRead.GetSingleAsync<Book, BookDTO>(x => x.ID.Equals(ID), true);
                if (bookDto == null)
                {
                    return NotFound();
                }

                var authorDto = await _dbRead.GetAsync<Author, AuthorDTO>(true);
                SelectList selectList = new SelectList(authorDto, "ID", "Name", authorDto.Single(x => x.ID.Equals(bookDto.Author_ID)));
                var bookVieModel = new BookViewModel()
                {
                    Book = bookDto,
                    AuthorsList = selectList
                };
                return View(bookVieModel);
            }
        }
        [HttpPost, ActionName("Delete")]
        public async Task<IActionResult> DeleteConfirmed(int? id)
        {

            if (id == null)
            {
                return NotFound();
            }
            else
            {
                try
                {
                    int ID = id.Value;
                    var bookDto = await _dbRead.GetSingleAsync<Book, BookDTO>(x => x.ID.Equals(ID), true);
                    if (bookDto == null)
                    {
                        return NotFound();
                    }

                    await _dbWrite.DeleteAsync<Book>(x => x.ID.Equals(ID));
                    return RedirectToAction(nameof(BookList));

                }
                catch (Exception)
                {
                    //await unit.RollbackAsync();
                    throw;
                }

            }
        }
        [HttpGet]
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }
            else
            {
                int ID = id.Value;
                var bookDto = await _dbRead.GetSingleAsync<Book, BookDTO>(x => x.ID.Equals(ID), true);
                if (bookDto == null)
                {
                    return NotFound();
                }

                var authorDto = await _dbRead.GetAsync<Author, AuthorDTO>(true);
                SelectList selectList = new SelectList(authorDto, "ID", "Name", authorDto.Single(x => x.ID.Equals(bookDto.Author_ID)));
                var bookVieModel = new BookViewModel()
                {
                    Book = bookDto,
                    AuthorsList = selectList
                };
                return View(bookVieModel);
            }
        }
    }
}