﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using VOD.Common.Entities;

namespace VOD.Database.Service
{
    public class UIReadService : IUIReadService
    {
        private readonly IDbReadService _dbRead;
        private readonly IMapper _mapper;
        public UIReadService(IDbReadService db, IMapper mapper)
        {
            _dbRead = db;
            _mapper = mapper;
        }

        public async Task<Book> GetBook(int? id, bool include)
        {
            if (include)
            {
                _dbRead.Include<Author>();
            }
            return await _dbRead.SingleAsync<Book>(x => x.ID.Equals(id.Value));
        }

        public async Task<IEnumerable<Book>> GetBooks(bool include)
        {
            if (include)
            {
                _dbRead.Include<Author>();
            }
            return await _dbRead.GetAcync<Book>();
        }
        public async Task<IEnumerable<Author>> GetAuthors()
        {
            return await _dbRead.GetAcync<Author>();
        }

        public async Task<List<TDestination>> GetAsync<TSource, TDestination>(bool include = false)
            where TSource : class
            where TDestination : class
        {
            try
            {
                if (include)
                {
                    _dbRead.Include<TSource>();
                }
                var entities = await _dbRead.GetAcync<TSource>();
                return _mapper.Map<List<TDestination>>(entities);
            }
            catch (SqlException ex)
            {

                return null;
            }
        }

        public async Task<TDestination> GetSingleAsync<TSource, TDestination>(Expression<Func<TSource, bool>> predicate, bool include = false)
            where TSource : class
            where TDestination : class
        {
            if (include)
            {
                _dbRead.Include<TSource>();
            }
            var entity = await _dbRead.SingleAsync<TSource>(predicate);
            return _mapper.Map<TDestination>(entity);
        }
    }
}
