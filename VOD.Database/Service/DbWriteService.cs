﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VOD.Database.Context;

namespace VOD.Database.Service
{
    public class DbWriteService : IDbWriteService
    {
        private readonly VODContext _db;

        public DbWriteService(VODContext db)
        {
            _db = db;
        }

        public async Task AddAsync<TEntity>(TEntity item) where TEntity : class
        {
            try
            {
                await _db.Set<TEntity>().AddAsync(item);
            }
            catch (Exception)
            {

                throw;
            }
        }

        public async Task<bool> Commit()
        {
            try
            {
                return await _db.SaveChangesAsync() >= 0;
            }
            catch (DbUpdateException ex)
            {
                throw;
            }
            catch (SqlException ex)
            {
                throw;
            }
        }

        public void Remove<TEntity>(TEntity item) where TEntity : class
        {
            try
            {
                _db.Set<TEntity>().Remove(item);
            }
            catch (Exception)
            {
                throw;
            }
        }
        public void Update<TEntity>(TEntity item) where TEntity : class
        {
            try
            {
                var entity = _db.Find<TEntity>(item.GetType().GetProperty("ID").GetValue(item));

                if (entity != null) _db.Entry(entity).State = EntityState.Detached;

                _db.Set<TEntity>().Update(item);
            }
            catch (Exception)
            {
                throw;
            }
        }
        private void RejectChanges()
        {
            foreach (var entry in _db.ChangeTracker.Entries()
                  .Where(e => e.State != EntityState.Unchanged))
            {
                switch (entry.State)
                {
                    case EntityState.Added:
                        entry.State = EntityState.Detached;
                        break;
                    case EntityState.Modified:
                    case EntityState.Deleted:
                        entry.Reload();
                        break;
                }
            }
        }
    }
}
