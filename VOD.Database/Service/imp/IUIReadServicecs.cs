﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using VOD.Common.Entities;

namespace VOD.Database.Service
{
    public interface IUIReadService
    {
        Task<IEnumerable<Book>> GetBooks(bool include = false);
        Task<Book> GetBook(int? id, bool include = false);

        Task<IEnumerable<Author>> GetAuthors();
        Task<List<TDestination>> GetAsync<TSource, TDestination>(bool include = false)
            where TSource : class where TDestination : class;
        Task<TDestination> GetSingleAsync<TSource, TDestination>(Expression<Func<TSource, bool>> predicate, bool include = false)
            where TSource : class where TDestination : class;
    }
}
