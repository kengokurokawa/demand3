﻿using AutoMapper;
using System;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace VOD.Database.Service
{
    public class UIWriteService : IUIWriteService
    {
        private readonly IDbWriteService _dbWrite;
        private readonly IMapper _mapper;

        private readonly IDbReadService _dbRead;
        public UIWriteService(IDbWriteService dbWrite, IMapper mapper, IDbReadService dbRead)
        {
            _dbWrite = dbWrite;
            _mapper = mapper;
            _dbRead = dbRead;
        }
        public async Task<int> CreateAsync<TSource, TDestination>(TSource item)
            where TSource : class
            where TDestination : class
        {
            try
            {
                var entity = _mapper.Map<TDestination>(item);
                await _dbWrite.AddAsync(entity);
                var succeeded = await _dbWrite.Commit();
                if (succeeded) return (int)entity.GetType()
                        .GetProperty("ID").GetValue(entity);
            }
            catch (Exception)
            {

                throw;
            }
            return -1;
        }

        //public async Task<bool> DeleteAsync<TSource, TDestination>(TSource item) where TSource : class
        //    where TDestination : class
        //{
        //    try
        //    {
        //        var entity = _mapper.Map<TDestination>(item);
        //        _dbWrite.Remove(entity);
        //        var succeeded = await _dbWrite.Commit();
        //        return succeeded;
        //    }
        //    catch (Exception)
        //    {

        //        throw;
        //    }
        //}

        public async Task<bool> DeleteAsync<TSource>(Expression<Func<TSource, bool>> predicate) where TSource : class
        {
            try
            {
                var entity = await _dbRead.SingleAsync<TSource>(predicate);

                _dbWrite.Remove(entity);
                var succeeded = await _dbWrite.Commit();
                return succeeded;
            }
            catch (Exception)
            {

                throw;
            }
;
        }

        public async Task<bool> UpdateAsync<TSource, TDestination>(TSource item)
            where TSource : class
            where TDestination : class
        {
            try
            {
                var entity = _mapper.Map<TDestination>(item);
                 _dbWrite.Update(entity);
                var succeeded = await _dbWrite.Commit();
                return succeeded;
            }
            catch (Exception)
            {

                throw;
            }
        }
    }
}
