﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using VOD.Common.DTOModel;
using VOD.Common.Entities;

namespace VOD.Database.Service
{
    public interface IUserService
    {
        Task<UserDTO> GetByEmailAsync(string email);
        Task<IEnumerable<UserDTO>> GetUsersAsync();
        Task<UserDTO> GetUserAsync(string userId);
        Task<VODUser> GetUserAsync(LoginUserDTO loginUser, bool isIncludeClaims);

        Task<IdentityResult> AddUserAsync(RegisterUserDTO user);
        Task<bool> UpdateUserAsync(UserDTO user);

        Task<bool> DeleteUserAsync(string userId);
    }
}
