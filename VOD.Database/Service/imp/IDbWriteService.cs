﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace VOD.Database.Service
{
    public interface IDbWriteService
    {
        Task<bool> Commit();

        Task AddAsync<TEntity>(TEntity item) where TEntity : class;
        void Remove<TEntity>(TEntity item) where TEntity : class;

        void Update<TEntity>(TEntity item) where TEntity : class;

    }
}
