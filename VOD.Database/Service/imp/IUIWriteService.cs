﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace VOD.Database.Service
{
    public interface IUIWriteService
    {
        Task<int> CreateAsync<TSource, TDestination>(TSource item)
            where TSource : class where TDestination : class;
        Task<bool> UpdateAsync<TSource, TDestination>(TSource item)
            where TSource : class where TDestination : class;
        Task<bool> DeleteAsync<TSource>(Expression<Func<TSource, bool>> predicate)
             where TSource : class;
    }
}
