﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using VOD.Database.Context;

namespace VOD.Database.Service
{
    public class DbReadService : IDbReadService
    {

        private readonly VODContext _context;

        public DbReadService(VODContext context)
        {
            _context = context;
        }

        public async Task<bool> AnyAsync<TEntity>(Expression<Func<TEntity, bool>> predicate) where TEntity : class
        {
            return await _context.Set<TEntity>().AnyAsync(predicate);
        }

        public async Task<IEnumerable<TEntity>> GetAcync<TEntity>() where TEntity : class
        {
            return await _context.Set<TEntity>().ToListAsync();
        }

        public async Task<IEnumerable<TEntity>> GetAcync<TEntity>(Expression<Func<TEntity, bool>> predicate) where TEntity : class
        {
            return await _context.Set<TEntity>().Where(predicate).ToListAsync();
        }

        public void Include<TEntity>() where TEntity : class
        {
            var propertyNames = _context.Model
                                .FindEntityType(typeof(TEntity))
                                .GetNavigations()
                                .Select(e => e.Name);

            foreach (var name in propertyNames)
            {
                _context.Set<TEntity>().Include(name).Load();
            }
        }

        public void Include<TEntity1, TEntity2>()
            where TEntity1 : class
            where TEntity2 : class
        {

            Include<TEntity1>();
            Include<TEntity2>();
        }

        public async Task<TEntity> SingleAsync<TEntity>(Expression<Func<TEntity, bool>> predicate) where TEntity : class
        {
            return await _context.Set<TEntity>().SingleOrDefaultAsync(predicate);
        }
    }
}
