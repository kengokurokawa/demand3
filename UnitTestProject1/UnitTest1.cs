using Microsoft.VisualStudio.TestTools.UnitTesting;
using VOD.Database.Service;
using Moq;
using System.Threading.Tasks;
using VOD.Common.Entities;

namespace UnitTestProject1
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void TestMethod_本一覧の取得()
        {
            int? id = 20;
            bool include = true;
            var mock = new Mock<IUIReadService>();

            mock.Setup(x => x.GetBook(20, true))
                .ReturnsAsync(new Book()
                {
                    ID = 20,
                    Title = "AA伝記0000000020",
                    Price = 300,
                    Publisher = "XX出版0000000020",
                    Sample = true,
                    ReleaseDate = new System.DateTime(2017, 01, 01),
                    Author_ID = 2,
                    Author = new Author()
                    {
                        ID = 2,
                        Name = "TEST"
                    }
                });

            var mock2 = new Mock<IUIReadService>();
            mock2.Setup(x => x.GetBook(20, false))
                 .ReturnsAsync(new Book()
                 {
                     ID = 20,
                     Title = "AA伝記0000000020",
                     Price = 300,
                     Publisher = "XX出版0000000020",
                     Sample = true,
                     ReleaseDate = new System.DateTime(2017, 01, 01),
                     Author_ID = 2,
                 });

            var pattern1 = mock.Object;
            var pattern2 = mock2.Object;

            var book = new Book()
            {
                ID = 20,
                Title = "AA伝記0000000020",
                Price = 300,
                Publisher = "XX出版0000000020",
                Sample = true,
                ReleaseDate = new System.DateTime(2017, 01, 01),
                Author_ID = 2
            };


            Assert.AreEqual(20, pattern1.GetBook(id, include).Result.ID);
            Assert.IsNotNull(pattern1.GetBook(id, true).Result.Author);
            Assert.IsNull(pattern2.GetBook(id, false).Result.Author);

            //Assert.AreEqual(20, con.GetBook(id, include).Result.ID);
            //Assert.AreEqual(10,10);
            //Assert.Equals(2,  mockBook.Author_ID);

        }
    }
}
