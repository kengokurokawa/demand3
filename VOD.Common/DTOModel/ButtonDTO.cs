﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VOD.Common.DTOModel
{
    public class ButtonDTO
    {
        public int Id { get; set; }
        public string UserId { get; set; }

        public ButtonDTO(string userId)
        {
            UserId = userId;
        }
        public ButtonDTO(int id)
        {
            this.Id = id;
        }
    }
}
