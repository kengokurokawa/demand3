﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace VOD.Common.DTOModel
{
    public class AuthorDTO
    {
        [Key]
        public int ID { get; set; }
        [DisplayName("著名")]
        public string Name { get; set; }
        public DateTime? CreateDate { get; set; }

        public DateTime? UpdateDate { get; set; }
    }
}
