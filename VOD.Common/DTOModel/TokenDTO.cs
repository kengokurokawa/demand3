﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VOD.Common.DTOModel
{
    public class TokenDTO
    {

        public string Token { get; set; } = "";
        public DateTime TokenExpires { get; set; } = default;

        public bool TokenHasExpired
        {
            get
            {
                return TokenExpires == default ? true :
                    !(TokenExpires.Subtract(DateTime.UtcNow).Minutes > 0);
            }
        }
        public TokenDTO(string token)
        {

        }

        public TokenDTO(string token, DateTime validTo)
        {
            Token = token;
            this.TokenExpires = validTo;
        }
    }
}
