﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace VOD.Common.Entities
{
    public class Author
    {
        [Key]
        public int ID { get; set; }
        public string Name { get; set; }
        public ICollection<Book> Books { get; set; }
        public DateTime? CreateDate { get; set; }

        public DateTime? UpdateDate { get; set; }
    }
}
