﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace VOD.Common.Entities
{
    public class Book
    {
        public int ID { get; set; }
        [DisplayName("書名")]
        [Required(ErrorMessage = "{0}は入力必須です。")]
        public string Title { get; set; }

        [DisplayName("価格")]
        [Required(ErrorMessage = "{0}は入力必須です。")]
        [Range(0, 50000, ErrorMessage = "{0}は{1}～{2}円以内で入力してください")]
        public int? Price { get; set; }

        [DisplayName("出版社")]
        public string Publisher { get; set; }

        [DisplayName("配布サンプル")]
        public bool Sample { get; set; }

        [DataType(DataType.Date)]
        [DisplayName("発売日")]
        public DateTime? ReleaseDate { get; set; }

        [Timestamp]
        public byte[] RowVersion { get; set; }

        [DataType(DataType.Date)]
        public DateTime? CreateDate { get; set; }

        [DataType(DataType.Date)]
        public DateTime? UpdateDate { get; set; }

        public int Author_ID { get; set; }

        [ForeignKey("Author_ID")]
        public virtual Author Author
        {
            get; set;


        }
    }
}
