﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace VOD.Common.Service
{
    public interface IUIReadService
    {
        Task<List<TDestination>> GetAsync<TSource, TDestination>(bool include = false)
    where TSource : class where TDestination : class;
        Task<TDestination> GetSingleAsync<TSource, TDestination>(Expression<Func<TSource, bool>> predicate, bool include = false)
            where TSource : class where TDestination : class;
    }
}
