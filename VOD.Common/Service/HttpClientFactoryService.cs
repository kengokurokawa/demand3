﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using VOD.Common.Exception;
using VOD.Common.Extensions;

namespace VOD.Common.Service
{
    public class HttpClientFactoryService : IHttpClientFactoryService
    {
        private readonly IHttpClientFactory _f;
        private CancellationTokenSource _cancellationTokenSource;
        private CancellationToken _cancellationToken;

        public HttpClientFactoryService(IHttpClientFactory f)
        {
            _f = f;
            _cancellationTokenSource = new CancellationTokenSource();
            _cancellationToken = _cancellationTokenSource.Token;
        }

        public async Task<List<TResponse>> GetListAsync<TResponse>(string uri, string serviceName, string token = "")
            where TResponse : class
        {
            try
            {
                if (new string[] { uri, serviceName }.IsNullOrEmptyOrWhiteSpace())
                {
                    throw new HttpResponseException(HttpStatusCode.NotFound, "Could not find the resource");
                }
                var httpClient = _f.CreateClient(serviceName);
                return await httpClient.GetListAsync<TResponse>(uri.ToLower(), _cancellationToken, token);
            }
            catch (System.Exception)
            {

                throw;
            }

        }
        public async Task<TResponse> GetAsync<TResponse>(string uri, string serviceName, string token = "")
    where TResponse : class
        {
            try
            {
                if (new string[] { uri, serviceName }.IsNullOrEmptyOrWhiteSpace())
                {
                    throw new HttpResponseException(HttpStatusCode.NotFound, "Could not find the resource");
                }
                var httpClient = _f.CreateClient(serviceName);
                return await httpClient.GetAsync<TResponse>(uri.ToLower(), _cancellationToken, token);
            }
            catch (System.Exception)
            {

                throw;
            }

        }

        public async Task<TResponse> PostAsync<TRequest, TResponse>(TRequest content, string uri, string serviceName, string token = "")
            where TRequest : class
            where TResponse : class
        {
            try
            {
                if (new string[] { uri, serviceName }.IsNullOrEmptyOrWhiteSpace())
                {
                    throw new HttpResponseException(HttpStatusCode.NotFound, "Could not find the resource");
                }
                var httpClient = _f.CreateClient(serviceName);
                return await httpClient.PostAsync<TRequest,TResponse>(uri.ToLower(),content,_cancellationToken, token);
            }
            catch (System.Exception)
            {

                throw;
            }
        }
        public async Task<TResponse> PutAsync<TRequest, TResponse>(TRequest content, string uri, string serviceName, string token = "")
         where TRequest : class
         where TResponse : class
        {
            try
            {
                if (new string[] { uri, serviceName }.IsNullOrEmptyOrWhiteSpace())
                {
                    throw new HttpResponseException(HttpStatusCode.NotFound, "Could not find the resource");
                }
                var httpClient = _f.CreateClient(serviceName);
                return await httpClient.PutAsync<TRequest, TResponse>(uri.ToLower(), content, _cancellationToken, token);
            }
            catch (System.Exception)
            {

                throw;
            }
        }
        public async Task<string> DeleteAsync(string uri, string serviceName, string token = "")
        {
            try
            {
                if (new string[] { uri, serviceName }.IsNullOrEmptyOrWhiteSpace())
                {
                    throw new HttpResponseException(HttpStatusCode.NotFound, "Could not find the resource");
                }
                var httpClient = _f.CreateClient(serviceName);
                return await httpClient.DeleteAsync(uri.ToLower(), _cancellationToken, token);
            }
            catch (System.Exception)
            {

                throw;
            }
        }
    }
}
