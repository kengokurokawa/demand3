﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace VOD.Common.Service
{
    public class ApiWriteService : IUIWriteService
    {
        private readonly IHttpClientFactoryService _http;

        private Dictionary<string, object> _properties = new Dictionary<string, object>();

        public ApiWriteService(IHttpClientFactoryService http)
        {
            _http = http;
        }

        public async Task<int> CreateAsync<TSource, TDestination>(TSource item)
            where TSource : class
            where TDestination : class
        {
            GetProperties(item);
            string uri = FormatUriWithIds<TDestination>();
            var response =  await _http.PostAsync<TSource,TDestination>(item,uri,"AdminClient");

            return (int)response.GetType().GetProperty("ID").GetValue(response);
        }

        public async Task<bool> DeleteAsync<TSource>(Expression<Func<TSource, bool>> predicate) where TSource : class
        {
            GetProperties(predicate);
            string uri = FormatUriWithIds<TSource>();
            var response = await _http.DeleteAsync(uri, "AdminClient");

            return true;
        }

        public async Task<bool> UpdateAsync<TSource, TDestination>(TSource item)
            where TSource : class
            where TDestination : class
        {
            GetProperties(item);
            string uri = FormatUriWithIds<TDestination>();
            var response = await _http.PutAsync<TSource, TDestination>(item, uri, "AdminClient");

            return true;
        }
        private string FormatUriWithIds<TSource>()
        {
            string uri = "api";
            object id;

            var succeeded = _properties.TryGetValue("ID", out id);
            if (succeeded)
            {
                uri = $"{uri}/{typeof(TSource).Name}/{id}";
            }
            else
            {
                uri = $"{uri}/{typeof(TSource).Name}";
            }
            return uri;

        }
        private string FormatUriWithoutIds<TSource>()
        {
            string uri = "api";

            uri = $"{uri}/{typeof(TSource).Name}";

            return uri;
        }
        private void GetProperties<TSource>(TSource source)
        {
            try
            {
                _properties.Clear();
                var idProperty = source.GetType().GetProperty("ID");

                if(idProperty != null)
                {
                    var id = idProperty.GetValue(source);
                    if (id != null && (int)id > 0) _properties.Add("ID",id);
                }
            }
            catch (System.Exception)
            {
                _properties.Clear();
                throw;
            }
        }
        private void GetProperties<TSource>(Expression<Func<TSource, bool>> expression)
        {
            try
            {
                var lamba = expression as LambdaExpression;
                _properties.Clear();

                ResolveExpression(lamba.Body);

                var typenName = typeof(TSource).Name;
                if (!typenName.Equals("Instructor") &&
                   !typenName.Equals("Course") &&
                   !_properties.ContainsKey("courseId"))
                {
                    _properties.Add("courseId", 0);
                }
            }
            catch (System.Exception)
            {

                throw;
            }
        }
        private void GetExpressionProperties(Expression expression)
        {
            var body = (MethodCallExpression)expression;
            var argument = body.Arguments[0];

            if (argument is MemberExpression)
            {
                var memberExpression = (MemberExpression)argument;

                var value = ((FieldInfo)memberExpression.Member).GetValue(
                    ((ConstantExpression)memberExpression.Expression).Value);

                _properties.Add(memberExpression.Member.Name, value);
            }

        }
        private void ResolveExpression(Expression expression)
        {
            try
            {
                if (expression.NodeType == ExpressionType.AndAlso)
                {
                    var binaryExpression = expression as BinaryExpression;
                    ResolveExpression(binaryExpression.Left);
                    ResolveExpression(binaryExpression.Right);
                }
                else if (expression is MethodCallExpression)
                {
                    GetExpressionProperties(expression);
                }
            }
            catch (System.Exception)
            {

                throw;
            }
        }
    }
}
