﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace VOD.Common.Service
{
    public interface IHttpClientFactoryService
    {
        Task<List<TResponse>> GetListAsync<TResponse>(string uri, string serviceName, string token = "")
            where TResponse : class;

        Task<TResponse> GetAsync<TResponse>(string uri, string serviceName, string token = "")
            where TResponse : class;

        Task<TResponse> PostAsync<TRequest,TResponse>(TRequest content,string uri, string serviceName, string token = "")
            where TRequest : class where TResponse : class;
        Task<TResponse> PutAsync<TRequest, TResponse>(TRequest content, string uri, string serviceName, string token = "")
    where TRequest : class where TResponse : class;
        Task<string> DeleteAsync(string uri, string serviceName, string token = "");


    }
}
