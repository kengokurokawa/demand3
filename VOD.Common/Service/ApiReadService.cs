﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace VOD.Common.Service
{
    public class ApiReadService : IUIReadService
    {
        private readonly IHttpClientFactoryService _http;

        private Dictionary<string, object> _properties = new Dictionary<string, object>();
        public ApiReadService(IHttpClientFactoryService http)
        {
            _http = http;
        }
        public async Task<List<TDestination>> GetAsync<TSource, TDestination>(bool include = false)
            where TSource : class
            where TDestination : class
        {

            string uri = FormatUriWithoutIds<TSource>();
            return await _http.GetListAsync<TDestination>($"{uri}?include={include.ToString()}", "AdminClient");
        }

        public async Task<TDestination> GetSingleAsync<TSource, TDestination>(Expression<Func<TSource, bool>> predicate, bool include = false)
            where TSource : class
            where TDestination : class
        {
            GetProperties(predicate);
            string uri = FormatUriWithIds<TSource>();
            return await _http.GetAsync<TDestination>($"{uri}?include={include.ToString()}", "AdminClient");
        }
        private string FormatUriWithIds<TSource>()
        {

            string uri = "api";
            object id;

            var succeeded = _properties.TryGetValue("ID", out id);
            if (succeeded)
            {
                uri = $"{uri}/{typeof(TSource).Name}/{id}";
            }
            else
            {
                uri = $"{uri}/{typeof(TSource).Name}";
            }
            return uri;

        }
        private string FormatUriWithoutIds<TSource>()
        {

            string uri = "api";

            uri = $"{uri}/{typeof(TSource).Name}";

            return uri;
        }
        private void GetProperties<TSource>(TSource source)
        {
            try
            {
                _properties.Clear();
                var idProperty = source.GetType().GetProperty("ID");

                if (idProperty != null)
                {
                    var id = idProperty.GetValue(source);
                    if (id != null && (int)id > 0) _properties.Add("id", id);
                }
            }
            catch (System.Exception)
            {
                _properties.Clear();
                throw;
            }
        }
        private void GetProperties<TSource>(Expression<Func<TSource, bool>> expression)
        {
            try
            {
                var lamba = expression as LambdaExpression;
                _properties.Clear();

                ResolveExpression(lamba.Body);

                var typenName = typeof(TSource).Name;
                if (!typenName.Equals("Instructor") &&
                   !typenName.Equals("Course") &&
                   !_properties.ContainsKey("courseId"))
                {
                    _properties.Add("courseId", 0);
                }
            }
            catch (System.Exception)
            {

                throw;
            }
        }
        private void GetExpressionProperties(Expression expression)
        {
            var body = (MethodCallExpression)expression;
            var argument = body.Arguments[0];

            if (argument is MemberExpression)
            {
                var memberExpression = (MemberExpression)argument;

                var value = ((FieldInfo)memberExpression.Member).GetValue(
                    ((ConstantExpression)memberExpression.Expression).Value);

                _properties.Add(memberExpression.Member.Name, value);
            }

        }
        private void ResolveExpression(Expression expression)
        {
            try
            {
                if (expression.NodeType == ExpressionType.AndAlso)
                {
                    var binaryExpression = expression as BinaryExpression;
                    ResolveExpression(binaryExpression.Left);
                    ResolveExpression(binaryExpression.Right);
                }
                else if (expression is MethodCallExpression)
                {
                    GetExpressionProperties(expression);
                }
            }
            catch (System.Exception)
            {

                throw;
            }
        }

        public Task<List<TDestination>> GetAsync<TSource, TDestination>(Expression<Func<TSource, bool>> predicate, bool include = false)
            where TSource : class
            where TDestination : class
        {
            throw new NotImplementedException();
        }
    }
}
