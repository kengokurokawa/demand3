﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VOD.Common.Extensions
{
    public static class StringExtensions
    {
        public static bool IsNullOrEmptyOrWhiteSpace(this string[] values)
        {

            foreach (var val in values)
            {
                if (string.IsNullOrEmpty(val) || string.IsNullOrWhiteSpace(val))
                {
                    return true;
                }
                return false;
            }


            return false;
        }
        public static bool IsNullOrEmptyOrWhiteSpace(this string value)
        {

            if (string.IsNullOrEmpty(value) || string.IsNullOrWhiteSpace(value))
            {
                return true;
            }
            return false;
        }
        public static string Truncate(this string value, int length)
        {

            if (value.IsNullOrEmptyOrWhiteSpace())
            {
                return "";
            }
            if (value.Length <= length) return value;

            return $"{value.Substring(0, length)}....";
        }
    }
}
