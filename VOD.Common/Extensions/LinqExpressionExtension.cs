﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Reflection;
using System.Text;
using VOD.Common.DTOModel;

namespace VOD.Common.Extensions
{
    public static class LinqExpressionExtension
    {
        private static Dictionary<string, object> _properties = new Dictionary<string, object>();


        //public static Expression<Func<TSource, bool>> GetExpressionTreeWhere<TSource>(string[] keywords)
        public static Expression<Func<TSource, bool>> GetExpressionTreeWhere<TSource>(object keywords)
        {

            var entityType = typeof(TSource);
            var propertyName = "ID";
            int ID1 = 11;
            int ID2 = 7;
            var parameter3 = Expression.Parameter(entityType, "entity");



            var exp1 = Expression.Equal(
               Expression.Property(parameter3, propertyName),
               Expression.Constant(ID1));

            var exp2 = Expression.Equal(
             Expression.Property(parameter3, "Title"),
             Expression.Constant("AA伝記0000000006"));



            var lambda = Expression.Lambda<Func<TSource, bool>>(
                    Expression.OrElse(exp1, exp2
                    )
                , parameter3);
            return lambda;
            //Expression<Func<TSource,bool>> el = y => y.
            // 引数が適切でない場合には、例外を発行する
            //int length = keywords.Length;
            //if (length == 0)
            //{
            //    throw new ArgumentException("キーワード指定なし。");
            //}

            // ラムダ式における左辺のパラメータの名前
            // （「item => ……」の部分に対応）
            const string paramName = "item";
            const string paramNameId = "item.ID";

            // ラムダ式の左辺を構成するパラメータ項目の1つを作成
            ParameterExpression parameter =
              LambdaUtil.GetStringParameterExpression<TSource>(paramName);

            // ラムダ式の左辺であるパラメータ（全項目）を配列にまとめる
            // （※ただし今回はパラメータ項目は1つしかない）
            ParameterExpression[] parameters =
              LambdaUtil.GetParameterExpressions(parameter);


            ParameterExpression parameter2 =
              LambdaUtil.GetStringParameterExpression(paramNameId);

            // ラムダ式の左辺であるパラメータ（全項目）を配列にまとめる
            // （※ただし今回はパラメータ項目は1つしかない）
            ParameterExpression[] parameters2 =
              LambdaUtil.GetParameterExpressions(parameter2);

            // ラムダ式の右辺である式を組み立てる
            //Expression body = null;
            //for (int i = 0; i < length; i++)
            //{
            //    // 「item.ToLower().Contains(keywords[0].ToLower())」
            //    // という式を作成して、2つ目以降のキーワードは
            //    // 条件OR演算子（||）で連結する
            //    body = LambdaUtil.GetEqualsExpression(
            //      parameter2, keywords[i], (i == 0) ? null : body);
            //}
            Expression body = null;
            body = LambdaUtil.GetEqualsExpression(
              parameter2, keywords, body);
            // 最後に、動的に作成したラムダ式から式ツリーを取得する
            return LambdaUtil.GetLambdaExpressionWhere<TSource>(parameters, body);
        }
        private static class LambdaUtil
        {
            /// <summary>
            /// String.ToLowerメソッド。
            /// </summary>
            private static readonly MethodInfo ToLower =
              typeof(string).GetMethod("ToLower", Type.EmptyTypes);

            /// <summary>
            /// String.Containsメソッド。
            /// </summary>
            //private static readonly MethodInfo Contains =
            //  typeof(string).GetMethod("Contains");


            /// <summary>
            /// String.Containsメソッド。
            /// </summary>
            private static readonly MethodInfo equals =
              typeof(Object).GetMethod("Equals", new[] { typeof(Object) });

            //    equals = typeof(Object).GetMethod("Equals",
            //                                new Type[] { typeof(Object)
            //} );



            /// <summary>
            /// ラムダ式におけるパラメータを作成する。
            /// </summary>
            /// <param name="paramName">パラメータ名</param>
            /// <returns>パラメータ式</returns>
            public static ParameterExpression GetStringParameterExpression(string paramName)
            {
                return Expression.Parameter(typeof(object), paramName);
            }

            public static ParameterExpression GetStringParameterExpression<TSource>(string paramName)
            {
                return Expression.Parameter(typeof(TSource), paramName);
            }

            /// <summary>
            /// ラムダ式におけるパラメータ式の配列を作成する。
            /// </summary>
            /// <param name="parameters">複数のパラメータ式</param>
            /// <returns>パラメータ式の配列</returns>
            public static ParameterExpression[] GetParameterExpressions(params ParameterExpression[] parameters)
            {
                return parameters;
            }

            /// <summary>
            /// 「x.Contains("keyword")」を条件OR演算子（||）で連結しながら式を作成する。
            /// </summary>
            /// <param name="parameter">ラムダ式の左にあるパラメータ</param>
            /// <param name="keyword">検索キーワード</param>
            /// <param name="curBody">現在の「x.Contains("keyword")」の表現文。初回はnullを指定。2回目以降は前回の戻り値を指定。</param>
            ///// <returns>「x.Contains("keyword")」を||演算子で連結した式</returns>
            //public static Expression GetContainsExpression(Expression parameter, string keyword, Expression curBody)
            //{
            //    var keywordValue = Expression.Constant(keyword, typeof(string));
            //    var newBody = Expression.Call(
            //      Expression.Call(parameter, ToLower),
            //     equals,
            //      Expression.Call(keywordValue, ToLower));
            //    if (curBody != null)
            //    {
            //        return Expression.OrElse(curBody, newBody);
            //    }
            //    return newBody;
            //}

            public static Expression GetEqualsExpression(Expression parameter, object keyword, Expression curBody)
            {
                var keywordValue = Expression.Constant(keyword, typeof(object));
                var newBody = Expression.Call(parameter, equals, keywordValue);
                  //Expression.Call(parameter),
                  //equals,
                  //Expression.Call(keywordValue, ToLower));
                if (curBody != null)
                {
                    return Expression.OrElse(curBody, newBody);
                }
                return newBody;
            }

            /// <summary>
            /// 動的に作成したラムダ式から式ツリー型オブジェクトを取得する。
            /// </summary>
            /// <param name="parameters">パラメータ式の配列（＝ラムダ式の左辺）</param>
            /// <param name="body">式／文（＝ラムダ式の右辺）</param>
            /// <returns>式ツリー型オブジェクト</returns>
            public static Expression<Func<TSource, bool>> GetLambdaExpressionWhere<TSource>(ParameterExpression[] parameters, Expression body)
            {
                return Expression.Lambda<Func<TSource, bool>>(body, parameters);
            }
        }

    }
}
